package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/urfave/cli"
)

var gitconfigPath = "/etc/gitconfig"

const (
	// EnvVarBundleContent is the env var for the CA bundle to import
	EnvVarBundleContent = "ADDITIONAL_CA_CERT_BUNDLE"

	// FlagBundleContent is the CLI flag for the CA bundle to import
	FlagBundleContent = "additional-ca-cert-bundle"

	// DefaultBundlePath is the default import path of the CA bundle
	DefaultBundlePath = "/etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem"
)

// bundle combines a CA bundle with the path where it is imported.
type bundle struct {
	content string // content is the CA bundle to import.
	path    string // path is where the CA bundle is imported.
}

func main() {
	app := cli.NewApp()

	app.Commands = []cli.Command{runCommand()}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func runCommand() cli.Command {
	return cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on the given Docker image and generate a file",
		Flags: []cli.Flag{
			cli.StringFlag{
				Name:   FlagBundleContent,
				Usage:  "Additional CA certs bundle to import",
				EnvVar: EnvVarBundleContent,
			},
		},
		Action: func(c *cli.Context) error {

			b := bundle{
				content: c.String(FlagBundleContent),
				path:    DefaultBundlePath,
			}

			if err := b.writeCACertFile(); err != nil {
				return err
			}

			return b.writeCACertPathToGitConfigFile()
		},
	}
}

func (b bundle) writeCACertFile() error {
	// create parent directory for the import path
	if err := os.MkdirAll(filepath.Dir(b.path), 0755); err != nil {
		return err
	}

	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(b.path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = fmt.Fprintln(f, b.content)
	return err
}

func (b bundle) writeCACertPathToGitConfigFile() error {
	gitconfigContent := fmt.Sprintf("[http] sslCAInfo = %s", b.path)

	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(gitconfigPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = fmt.Fprintln(f, gitconfigContent)
	return err
}
